<?php

function password_update_settings($form) {
	$form['password_update_prefix'] = array(
		'#type' => 'textfield',
		'#title' => t('Password Prefix'),
		'#description' => t('Prefix to strip off of password hashes'),
		'#default_value' => variable_get('password_update_prefix', ''),
		'#attributes' => array(
			'placeholder' => 'magento_',
		),
	);

	$form['password_update_php'] = array(
		'#type' => 'textarea',
		'#rows' => 6,
		'#title' => t('PHP Conversion Code'),
		'#description' => t('Return TRUE if $plain_text matches $hashed_password'),
		'#default_value' => variable_get('password_update_php', ''),
		'#attributes' => array(
			'placeholder' => "\$plain_text .= \"2\";\r\nif (\$plain_text == \$hashed_password) {\n\t\$return TRUE;\n}",
		)
	);

	return system_settings_form($form);
}